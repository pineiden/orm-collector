export UVICORN_APP="orm_collector.api.app:app"
export UVICORN_SOCKET="/home/collector/socket/web.sock"
export UVICORN_WORKERS=3
export UVICORN_LOOP="uvloop"
export UVICORN_LOG="/home/collector/log/orm_collector_access.log"
export UVICORN_LOG_LEVEL="info"
