export USER=david
export GUNICORN_APP="orm_collector.api.app:app"
export GUNICORN_SOCKET="unix:/home/${USER}/socket/web.sock"
export GUNICORN_WORKERS=3
export GUNICORN_ACCESS_LOG="/home/${USER}/log/orm_collector_access.log"
export GUNICORN_ERROR_LOG="/home/${USER}/log/orm_collector_error.log"
export GUNICORN_LOG_LEVEL="info"
export GUNICORN_KERNEL="orm_collector.api.worker.ORMUvicornWorker"
