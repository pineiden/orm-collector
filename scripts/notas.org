#+TITLE: Servicio ORM Collector API

* Montar servicio Systemctl

Partiendo con los parámetros de configuracion

#+begin_src shell :results output
cat env-file.py
#+end_src

#+RESULTS:
: UVICORN_APPP="orm_collector.api.app:app"
: UVICORN_SOCKET="/home/${USER}/socket/web.sock"
: UVICORN_WORKERS=3
: UVICORN_LOOP="uvloop"
: UVICORN_LOG="/home/${USER}/log/orm_collector_access.log"
: UVICORN_LOG_LEVEL="info"

Y el archivo de systemctl

#+begin_src shell :results output
cat uvicorn.conf
#+end_src

#+RESULTS:
#+begin_example
[Unit]
Description=ORM Collector API
After=network.target

[Service]
User=web
Group=web
Environment="ENV_FILE=scripts/env-file.py"
EnvironmentFile="scripts/env-file.py"
WorkingDirectory=/home/collector/proyectos/orm-collector
RestartSec=1
ExecStart=/home/web/.virtualenvs/web/bin/uvicorn ${UVICORN_APP} \
												 --uds ${UVICORN_SOCKET} \
												 --workers ${UVICORN_WORKERS} \
												 --loop ${UVICORN_LOOP}
												 --access-log ${UVICORN_LOG}
												 --log-level ${UVICCORN_LOG_LEVEL}
												 
ExecStop=/bin/kill -WINCH ${MAINPID}
KillSignal=SIGCONT
Restart=always
StartLimitBurst=10

[Install]
WantedBy=multi-user.target
#+end_example

Crear un /enlace simbolico/ en el directorio de *systemctl*.

#+begin_src shell
ln -s $ORIGEN $DESTINO
#+end_src

* Montar Nginx Proxy

Se debe copiar el contenido (adaptado a caso) del archivo.

#+begin_src shell  :results output
cat nginx-proxy.conf
#+end_src

#+RESULTS:
#+begin_example
map $status $loggable {
    ~^[23]  0;
    default 1;
}

server {
  access_log /home/collector/web_log/ssl_access.log combined if=$loggable;
  error_log  /home/collector/web_log/ssl_error.log error;
  client_max_body_size 4G;

  listen 80;
  server_name 10.54.218.196;
  location / {
    proxy_pass http://orm;
    proxy_set_header Host $host;
  }
}
upstream orm {
  server unix:/home/collector/socket/web.sock;
}

#+end_example
