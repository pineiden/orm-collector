# ORM Collector Schemma

Este módulo consiste en la descripción de modelos de tablas
y sus relaciones, las clases que administran los datos de manera 
sencilla entregando una API, y las herramientas de creación del
esquema.

# Tecnologías que usa

- Sqlalchemy
- GeoSqlalchemy
- Posgresql
- Python3

# Modo de instalación

Primero, deberás clonar el proyecto:

~~~
git clone http://gitlab.csn.uchile.cl/dpineda/orm_collector.git
~~~

Luego, instalar en modo develop dentro de tu ambiente
virtual

~~~
python setup.py develop
~~~

